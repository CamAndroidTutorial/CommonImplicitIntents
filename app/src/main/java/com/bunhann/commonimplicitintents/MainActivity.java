package com.bunhann.commonimplicitintents;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    private Button btnCall, btnSendMail;
    private EditText phoneNumber;

    public static final String TAG = "MainActivity";

    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_CALL = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCall = findViewById(R.id.btnCallNow);
        btnSendMail = findViewById(R.id.btnSendMail);
        phoneNumber = findViewById(R.id.edPhone);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + String.valueOf(phoneNumber.getText())));
                if (callIntent.resolveActivity(getPackageManager()) != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            requestPermissions( new String[]{Manifest.permission.CALL_PHONE},
                                    MY_PERMISSIONS_REQUEST_READ_PHONE_CALL);
                            return;
                        }
                    }
                    startActivity(callIntent);
                }
            }
        });


        btnSendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "some@email.address" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, "Choose Your favorite email client."));
                }
            }
        });


    }

    public void btnSMSOnClick(View view){
        String to = "012268255";
        String message = "I'm Android Learner.";
        Uri smsUri = Uri.parse("tel:" + to);
        Intent intent = new Intent(Intent.ACTION_VIEW, smsUri);
        intent.putExtra("address", to);
        intent.putExtra("sms_body", message);
        intent.setType("vnd.android-dir/mms-sms");//here setType will set the previous data null.
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    public void btnMapOnClick(View view){
        String latitude = "11.549989";
        String longitude = "104.9249251";
        String zoomLevel = "14";
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        String data = String.format("geo:%s,%s", latitude, longitude);
        if (zoomLevel != null) {
            data = String.format("%s?z=%s", data, zoomLevel);
        }
        intent.setData(Uri.parse(data));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_CALL){
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for CALL_PHONE permission.
            Log.i(TAG, "Received response for CALL_PHONE permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                Log.i(TAG, "CALL_PHONE permission has now been granted. Showing preview.");
                Toast.makeText(this, "CALL_PHONE Permission has been granted.",
                        Toast.LENGTH_SHORT).show();
            } else {
                Log.i(TAG, "CALL_PHONE permission was NOT granted.");
                Toast.makeText(this, "Fail to get CALL_PHONE Permission!!!!",
                        Toast.LENGTH_SHORT).show();

            }
            // END_INCLUDE(permission_result)
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}
